#!/bin/bash

package_list=(
  patch
  gawk
  g++
  gcc
  autoconf
  automake
  bison
  libc6-dev
  libffi-dev
  libgdbm-dev
  libncurses5-dev
  libsqlite3-dev
  libtool
  libyaml-dev
  make
  patch
  pkg-config
  sqlite3
  zlib1g-dev
  libgmp-dev
  libreadline-dev
  libssl-dev
  firefox
  xrdp
  mate-core
  mate-desktop-environment
  mate-notification-daemon
)

sudo apt-get update
sudo apt-get install -y ${package_list[@]}

sudo sed -i.bak '/fi/a #xrdp multiple users configuration \n mate-session \n' /etc/xrdp/startwm.sh
sudo ufw allow 3389/tcp
sudo /etc/init.d/xrdp restart